(function() {
  /* global angular */
  
  function podrobnostiLokacijeCtrl($scope, $routeParams, $location, $uibModal, edugeocachePodatki, avtentikacija, geolokacija) {
    var vm = this;
    vm.idLokacije = $routeParams.idLokacije;
    
    vm.jePrijavljen = avtentikacija.jePrijavljen();
    
    vm.prvotnaStran = $location.path();
    
    edugeocachePodatki.podrobnostiLokacijeZaId(vm.idLokacije).then(
      function success(odgovor) {
        vm.podatki = { lokacija: odgovor.data };
        vm.glavaStrani = {
          naslov: vm.podatki.lokacija.naziv
        };
      },
      function error(odgovor) {
        console.log(odgovor.e);
      }
    );

    vm.pridobiPodatke = function(lokacija) {
      var lat = lokacija.coords.latitude,
          lng = lokacija.coords.longitude;
          
      vm.sporocilo = "Iščem bližnje lokacije.";
      edugeocachePodatki.koordinateTrenutneLokacije(lat, lng).then(
        function success(odgovor) {
          var razdalja = 0;
          vm.lat = lat;
          vm.lng = lng;
          for (var i in odgovor.data) {
            if (odgovor.data[i]._id === vm.idLokacije)
              vm.razdalja = odgovor.data[i].razdalja
              console.log(odgovor.data[i])
          }
          vm.data = { lokacije: odgovor.data };
          //console.log(vm.lat, vm.lng)
        }, 
        function error(odgovor) {
          vm.sporocilo = "Prišlo je do napake!";
          console.log(odgovor.e);
        });
    };
    geolokacija.vrniLokacijo(vm.pridobiPodatke, vm.prikaziNapako, vm.niLokacije);
    
    vm.prikaziPojavnoOknoObrazca = function() {
      var primerekModalnegaOkna = $uibModal.open({
        templateUrl: '/komentarModalnoOkno/komentarModalnoOkno.pogled.html',
        controller: 'komentarModalnoOkno',
        controllerAs: 'vm',
        resolve: {
          podrobnostiLokacije: function() {
            return {
              idLokacije: vm.idLokacije,
              nazivLokacije: vm.podatki.lokacija.naziv,
              razdalja: vm.razdalja
            };
          }
        }
      });
      
      primerekModalnegaOkna.result.then(function(podatki) {
        if (typeof podatki != 'undefined')
          vm.podatki.lokacija.komentarji.push(podatki);
      }, function() {
        // Ulovi dogodek in ne naredi ničesar
      });
    };
  }
  podrobnostiLokacijeCtrl.$inject = ['$scope', '$routeParams', '$location', '$uibModal', 'edugeocachePodatki', 'avtentikacija', 'geolokacija'];
  
  angular
    .module('edugeocache')
    .controller('podrobnostiLokacijeCtrl', podrobnostiLokacijeCtrl);
})();