(function() {
  /* global angular */
  
  function komentarModalnoOkno($uibModalInstance, edugeocachePodatki, podrobnostiLokacije) {
    var vm = this;
    
    vm.podrobnostiLokacije = podrobnostiLokacije;
    
    console.log(vm.podrobnostiLokacije.razdalja)
    
    vm.modalnoOkno = {
      zapri: function(odgovor) {
        $uibModalInstance.close(odgovor);
      },
      preklici: function() {
        $uibModalInstance.close();
      }
    };
    
    vm.dodajKomentar = function(idLokacije, podatkiObrazca, razdalja) {
      edugeocachePodatki.dodajKomentarZaId(idLokacije, {
        naziv: podatkiObrazca.naziv,
        ocena: podatkiObrazca.ocena,
        komentar: podatkiObrazca.komentar,
        razdalja: razdalja
      }).then(
        function success(odgovor) {
          vm.modalnoOkno.zapri(odgovor.data);
        },
        function error(odgovor) {
          vm.napakaNaObrazcu = "Napaka pri shranjevanju komentarja, poskusite znova!";
        }
      );
      return false;
    }
    
    vm.posiljanjePodatkov = function() {
      vm.napakaNaObrazcu = "";
      if (!vm.podatkiObrazca.ocena || !vm.podatkiObrazca.komentar) {
        vm.napakaNaObrazcu = "Prosim, izpolnite vsa vnosna polja!";
        return false;
      } else {
        vm.dodajKomentar(vm.podrobnostiLokacije.idLokacije, vm.podatkiObrazca, vm.podrobnostiLokacije.razdalja);
      }
    };
  };
  komentarModalnoOkno.$inject = ['$uibModalInstance', 'edugeocachePodatki', 'podrobnostiLokacije'];
  
  angular
    .module('edugeocache')
    .controller('komentarModalnoOkno', komentarModalnoOkno);
})();